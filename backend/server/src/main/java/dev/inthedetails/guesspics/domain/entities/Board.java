package dev.inthedetails.guesspics.domain.entities;

import dev.inthedetails.guesspics.domain.business.exception.BusinessViolationException;
import dev.inthedetails.guesspics.domain.configuration.BoardConfiguration;
import lombok.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@EqualsAndHashCode
@ToString
@Getter
@Setter
public class Board {

    public Board(@NonNull Match match, @NonNull BoardConfiguration boardConfiguration, @NonNull Set<Picture> pictures) {
        this.configuration = boardConfiguration;
        this.match = match;
        fillBoard(pictures);
    }

    @NonNull
    @ToString.Exclude
    private Match match;

    @NonNull
    @EqualsAndHashCode.Exclude
    private BoardConfiguration configuration;

    @EqualsAndHashCode.Exclude
    private Map<Cell, Picture> boardState;

    protected void fillBoard(@NonNull Set<Picture> pictures) {
        if (getConfiguration().getUnassignedPics() % 2 == 0) {
            throw new BusinessViolationException("Can't have an even number of unassigned Pics.");
        }
        if (getConfiguration().getTotalPics() - getConfiguration().getUnassignedPics() < 11) {
            throw new BusinessViolationException("Can't have less than 11 total team assigned Pics (no fun in easy guessin').");
        }
        if (pictures.size() != getConfiguration().getTotalPics()) {
            throw new BusinessViolationException("Board must have enough Pics to assign.");
        }
        List<Picture> pictureList = randomizeAndAssignPictures(pictures);
        prepareBoardState(pictureList);
    }

    /**
     * Assigns a {@link PictureType} for every {@link Picture} of the board
     * and returns a list properly filled with all kinds of {@link PictureType}s
     * and randomized for the game.
     * @param pictures the {@link Picture}s contained in the {@link Board}
     * @return List of {@link Picture}s, filled and randomized
     */
    protected List<Picture> randomizeAndAssignPictures(Set<Picture> pictures) {
        int totalCells = pictures.size();
        int teamAssignedPicsCount = totalCells - getConfiguration().getUnassignedPics();
        List<Picture> pictureList = pictures.stream().collect(Collectors.toList());
        Collections.shuffle(pictureList); // First shuffle to ensure no natural order affects the pics

        pictureList.get(0).setType(PictureType.BOMB); // arming the bomb

        // Assigning neutral Pics from 1 to UnassignedPics count
        assignPicsInRange(pictureList, PictureType.NEUTRAL, 1, getConfiguration().getUnassignedPics());

        // Assigning Pics for non-starting team
        int lastIndexNonStartingTeam = (teamAssignedPicsCount / 2) + getConfiguration().getUnassignedPics();
        assignPicsInRange(pictureList, match.getStartingTeam().getOppositeTeam().toPictureType(), getConfiguration().getUnassignedPics(), lastIndexNonStartingTeam);

        // Assigning Pics for starting team
        assignPicsInRange(pictureList, match.getStartingTeam().toPictureType(), lastIndexNonStartingTeam, totalCells);

        // Shuffling pictures again to randomize board
        Collections.shuffle(pictureList);
        return pictureList;
    }

    /**
     * Assigns {@link PictureType}s to {@link Picture}s frmo a list within a given range
     * @param pictureList list of {@link Picture}s to iterate
     * @param type {@link PictureType} to be assigned to {@link Picture}s in range
     * @param rangeStartInclusive start of the range (will be assigned)
     * @param rangeEndExclusive end of the range (will not be assigned)
     */
    protected void assignPicsInRange(List<Picture> pictureList, PictureType type, int rangeStartInclusive, int rangeEndExclusive) {
        IntStream.range(rangeStartInclusive, rangeEndExclusive).forEach(pictureIndex -> {
            pictureList.get(pictureIndex).setType(type);
        });
    }

    /**
     * Prepares the board with a {@link Picture} List
     * @param pictureList List of randomized and assigned {@link Picture}s
     */
    protected void prepareBoardState(List<Picture> pictureList) {
        int totalCells = pictureList.size();
        boardState = new HashMap<>(totalCells);
        IntStream.range(0, getConfiguration().getColumns()).forEach(column -> {
            IntStream.range(0, getConfiguration().getRows()).forEach(row -> {
                Cell cell = Cell.builder()
                        .board(this)
                        .column(column)
                        .row(row)
                        .build();
                Picture picture = pictureList.remove(0);
                picture.setGuessed(false);
                boardState.put(cell, picture);
            });
        });
    }

    public Set<Picture> getPicturesOfType(PictureType type) {
        return getBoardState().values().stream().filter(picture -> {
            return type.equals(picture.getType());
        }).collect(Collectors.toSet());
    }

}
