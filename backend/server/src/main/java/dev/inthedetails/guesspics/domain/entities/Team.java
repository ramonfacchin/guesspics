package dev.inthedetails.guesspics.domain.entities;

import lombok.*;

import java.util.Set;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Team {

    @NonNull
    private TeamType type;

    @NonNull
    @ToString.Exclude
    private Match match;

    @EqualsAndHashCode.Exclude
    private Set<Player> players;

    @EqualsAndHashCode.Exclude
    private Player currentSpeaker;

    @EqualsAndHashCode.Exclude
    private Integer score;

}
