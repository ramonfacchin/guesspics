package dev.inthedetails.guesspics.domain.entities;

import lombok.*;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Picture {

    @NonNull
    private String imageUri;

    @EqualsAndHashCode.Exclude
    private PictureType type;

    @EqualsAndHashCode.Exclude
    private Boolean guessed;

}
