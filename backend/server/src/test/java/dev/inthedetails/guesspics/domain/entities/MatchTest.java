package dev.inthedetails.guesspics.domain.entities;

import dev.inthedetails.guesspics.domain.business.exception.BusinessViolationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import java.util.Set;

class MatchTest {

    @Test
    @DisplayName("[construction] must have 4 players or more")
    public void match_construction_mustHave4PlayersOrMore() throws Exception {
        Assertions.assertThrows(BusinessViolationException.class, () -> {
            new Match("1", EntityTestFixtures.createPlayerSet(0));
        }, "Expected to throw BusinessViolationException when attempting to construct Match with no players.");
        Assertions.assertThrows(BusinessViolationException.class, () -> {
            new Match("2", EntityTestFixtures.createPlayerSet(1));
        }, "Expected to throw BusinessViolationException when attempting to construct Match with 1 player.");
        Assertions.assertThrows(BusinessViolationException.class, () -> {
            new Match("3", EntityTestFixtures.createPlayerSet(2));
        }, "Expected to throw BusinessViolationException when attempting to construct Match with 2 players.");
        Assertions.assertThrows(BusinessViolationException.class, () -> {
            new Match("4", EntityTestFixtures.createPlayerSet(3));
        }, "Expected to throw BusinessViolationException when attempting to construct Match with 3 players.");
    }

    @Test
    @DisplayName("[construction] must have 2 teams of different and coherent types")
    public void match_construction_mustHaveTeams() throws Exception {
        // Given
        Set<Player> players = EntityTestFixtures.createPlayerSet(6);

        // When
        Match match = new Match("1", players);

        // Then
        assertThat(match.getTeams().size(), is(2));
        assertThat(match.getTeams().get(TeamType.LEFT), hasProperty("type", equalTo(TeamType.LEFT)));
        assertThat(match.getTeams().get(TeamType.RIGHT), hasProperty("type", equalTo(TeamType.RIGHT)));
    }

    @Test
    @DisplayName("[construction] must evenly distribute players between teams if even number of players")
    public void match_construction_mustEvenlyDistributePlayers() throws Exception {
        // Given (a set of players)
        Set<Player> players = EntityTestFixtures.createPlayerSet(4);

        // When (match is created)
        Match match = new Match("1", players);
        int leftTeamSize = match.getTeams().get(TeamType.LEFT).getPlayers().size();
        int rightTeamSize = match.getTeams().get(TeamType.RIGHT).getPlayers().size();

        // Then (players must be evenly distributed between teams)
        assertThat(leftTeamSize, equalTo(rightTeamSize));
    }

    @Test
    @DisplayName("[construction] must almost evenly distribute players between teams if odd number of players (size difference == 1)")
    public void match_construction_mustDistributePlayersAsEvenAsPossibleWithOddNumberOfPlayers() throws Exception {
        // Given (a set of players)
        Set<Player> players = EntityTestFixtures.createPlayerSet(5);

        // When (match is created)
        Match match = new Match("1", players);
        int sizeDifference = match.getTeams().get(TeamType.LEFT).getPlayers().size() - match.getTeams().get(TeamType.RIGHT).getPlayers().size();

        // Then (players must be evenly distributed between teams)
        assertThat(sizeDifference, allOf(greaterThanOrEqualTo(-1), lessThanOrEqualTo(1)));
    }

}