package dev.inthedetails.guesspics.domain.entities;

public enum TeamType {
    LEFT,RIGHT;

    public TeamType getOppositeTeam() {
        return this.equals(LEFT) ? RIGHT : LEFT;
    }

    public PictureType toPictureType() {
        return PictureType.valueOf(this.name());
    }
}
