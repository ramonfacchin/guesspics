package dev.inthedetails.guesspics.domain.business.exception;

/**
 * Umbrella exception for Business Rules Violations.
 */
public class BusinessViolationException extends RuntimeException {

    public BusinessViolationException(String message) {
        super(message);
    }

    public BusinessViolationException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessViolationException(Throwable cause) {
        super(cause);
    }
}
