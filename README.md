# Guesspics

This repository contains a personal take on a Team Picture Guessing game inspired in Codenames Pictures
and is a study subject for some programming tools, frameworks and techniques.

## Frontend

React and Javascript based. See `frontend` folder.

## Backend

Java based. See `backend` folder.

## Misc

[TODO]
