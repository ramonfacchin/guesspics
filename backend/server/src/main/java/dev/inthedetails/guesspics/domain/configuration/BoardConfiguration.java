package dev.inthedetails.guesspics.domain.configuration;

/**
 * Interface with board configuration for GuessPics games.
 */
public interface BoardConfiguration {

    /**
     * @return amount of columns containing pictures
     */
    default Integer getColumns() {
        return 5;
    }

    /**
     * @return amount of rows containing pictures
     */
    default Integer getRows() {
        return 4;
    }

    /**
     * @return how many pictures the {@link dev.inthedetails.guesspics.domain.entities.Board} has
     */
    default Integer getTotalPics() {
        return getColumns() * getRows();
    }

    /**
     * @return amount of pictures not owned by any team.
     * this includes both neutral pics and the bomb and
     * must always be an odd value
     */
    default Integer getUnassignedPics() {
        return 5;
    }

    /**
     * Default implementation of {@link BoardConfiguration}
     */
    final class Default implements BoardConfiguration {
        private Default() {
        }

        public static Default newInstance() {
            return new Default();
        }
    }

}
