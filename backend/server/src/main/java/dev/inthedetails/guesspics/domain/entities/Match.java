package dev.inthedetails.guesspics.domain.entities;

import dev.inthedetails.guesspics.domain.business.exception.BusinessViolationException;
import lombok.*;

import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Match {

    public Match(@NonNull String identifier, @NonNull Set<Player> players) {
        if (players.size() < 4) {
            throw new BusinessViolationException("Can't start a match with less than 4 players.");
        }
        this.startingTeam = randomizeStartingTeam();
        initTeams();
        populateTeams(players);
    }

    @NonNull
    private String identifier;

    @NonNull
    @EqualsAndHashCode.Exclude
    private Integer rounds;

    @EqualsAndHashCode.Exclude
    private TeamType startingTeam;

    @EqualsAndHashCode.Exclude
    private Map<TeamType, Team> teams;

    @EqualsAndHashCode.Exclude
    private Board board;

    protected void initTeams() {
        this.teams = new HashMap<>();
        getTeams().put(TeamType.LEFT, Team.builder().match(this).type(TeamType.LEFT).players(new HashSet<>()).build());
        getTeams().put(TeamType.RIGHT, Team.builder().match(this).type(TeamType.RIGHT).players(new HashSet<>()).build());
    }

    protected void populateTeams(@NonNull Set<Player> players) {
        List<Player> playerList = players.stream().collect(Collectors.toList());
        Collections.shuffle(playerList);
        getTeams().get(TeamType.LEFT).getPlayers().addAll(playerList.subList(0, playerList.size() / 2));
        getTeams().get(TeamType.RIGHT).getPlayers().addAll(playerList.subList(playerList.size() / 2, playerList.size()));
    }

    protected TeamType randomizeStartingTeam() {
        return TeamType.values()[(int) (Math.round(Math.random()))];
    }

}
