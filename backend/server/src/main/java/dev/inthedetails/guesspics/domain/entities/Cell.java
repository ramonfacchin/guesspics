package dev.inthedetails.guesspics.domain.entities;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Cell {

    @NonNull
    private Integer column;

    @NonNull
    private Integer row;

    @NonNull
    @ToString.Exclude
    private Board board;
}
