package dev.inthedetails.guesspics.domain.entities;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

public class EntityTestFixtures {

    protected static Set<Player> createPlayerSet(int numberOfPlayers) {
        Set<Player> players = new HashSet<>();
        IntStream.range(0, numberOfPlayers).forEach(i -> {
            players.add(Player.builder()
                    .identifier(i+"")
                    .name(i+"")
                    .build());
        });
        return players;
    }

    protected static Set<Picture> createPictureSetForBoard(int numberOfPictures) {
        Set<Picture> pictures = new HashSet<>();
        IntStream.range(0, numberOfPictures).forEach(i -> {
            pictures.add(Picture.builder()
                    .imageUri(i+"")
                    .guessed(false)
                    .build());
        });
        return pictures;
    }
}
