package dev.inthedetails.guesspics.domain.entities;

import lombok.*;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Player {

    @NonNull
    private String identifier;

    @ToString.Exclude
    private Team team;

    @NonNull
    @EqualsAndHashCode.Exclude
    private String name;

}
