package dev.inthedetails.guesspics.domain.entities;

public enum PictureType {
    LEFT,RIGHT,NEUTRAL,BOMB;
}
