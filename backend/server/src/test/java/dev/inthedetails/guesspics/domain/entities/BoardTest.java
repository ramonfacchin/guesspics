package dev.inthedetails.guesspics.domain.entities;

import dev.inthedetails.guesspics.domain.configuration.BoardConfiguration;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

class BoardTest {

    @Test
    @DisplayName("[construction] every picture must have a type assigned")
    void randomizeAndAssignPictures_everyPicHasTypeAssigned() {
        // Given
        Match match = new Match("1", EntityTestFixtures.createPlayerSet(6));
        BoardConfiguration.Default boardConfiguration = BoardConfiguration.Default.newInstance();

        // When
        Board board = new Board(match, boardConfiguration, EntityTestFixtures.createPictureSetForBoard(boardConfiguration.getTotalPics()));

        // Then
        assertThat(board.getBoardState().values(), everyItem(hasProperty("type", notNullValue())));
    }

    @Test
    @DisplayName("[construction] starting team must have exactly one assigned pic more than non-starting team")
    void randomizeAndAssignPictures_startingTeamMorePics() {
        // Given
        Match match = new Match("1", EntityTestFixtures.createPlayerSet(6));
        BoardConfiguration.Default boardConfiguration = BoardConfiguration.Default.newInstance();

        // When
        Board board = new Board(match, boardConfiguration, EntityTestFixtures.createPictureSetForBoard(boardConfiguration.getTotalPics()));
        Set<Picture> startingTeamPictures = board.getPicturesOfType(match.getStartingTeam().toPictureType());
        Set<Picture> nonStartingTeamPictures = board.getPicturesOfType(match.getStartingTeam().getOppositeTeam().toPictureType());

        // Then
        assertThat(startingTeamPictures.size(), is(equalTo(nonStartingTeamPictures.size()+1)));
    }

    @Test
    @DisplayName("[construction] only one bomb must be placed")
    void randomizeAndAssignPictures_onlyOneBomb() {
        // Given
        Match match = new Match("1", EntityTestFixtures.createPlayerSet(6));
        BoardConfiguration.Default boardConfiguration = BoardConfiguration.Default.newInstance();

        // When
        Board board = new Board(match, boardConfiguration, EntityTestFixtures.createPictureSetForBoard(boardConfiguration.getTotalPics()));
        List<Picture> bombs = board.getBoardState().values().stream().filter(picture -> PictureType.BOMB.equals(picture.getType())).collect(Collectors.toList());

        // Then
        assertThat(bombs.size(), is(1));
    }

    @Test
    @DisplayName("[construction] number of neutral pics must be even")
    void randomizeAndAssignPictures_evenNumberOfNeutralPics() {
        // Given
        Match match = new Match("1", EntityTestFixtures.createPlayerSet(6));
        BoardConfiguration.Default boardConfiguration = BoardConfiguration.Default.newInstance();

        // When
        Board board = new Board(match, boardConfiguration, EntityTestFixtures.createPictureSetForBoard(boardConfiguration.getTotalPics()));
        List<Picture> neutralPics = board.getBoardState().values().stream().filter(picture -> PictureType.NEUTRAL.equals(picture.getType())).collect(Collectors.toList());

        // Then
        assertThat(neutralPics.size() % 2, is(0));
    }

    @Test
    @DisplayName("[construction] boardState map must have the same size as the input pics list")
    void prepareBoardState_sameSizeAsPictureList() {
        // Given
        Match match = new Match("1", EntityTestFixtures.createPlayerSet(6));
        BoardConfiguration.Default boardConfiguration = BoardConfiguration.Default.newInstance();

        // When
        Set<Picture> pictureSetForBoard = EntityTestFixtures.createPictureSetForBoard(boardConfiguration.getTotalPics());
        Board board = new Board(match, boardConfiguration, pictureSetForBoard);

        // Then
        assertThat(pictureSetForBoard.size(), equalTo(pictureSetForBoard.size()));
    }

    @Test
    @DisplayName("[construction] all pics must start unguessed")
    void prepareBoardState_allPicsMustBeUnguessed() {
        // Given
        Match match = new Match("1", EntityTestFixtures.createPlayerSet(6));
        BoardConfiguration.Default boardConfiguration = BoardConfiguration.Default.newInstance();

        // When
        Board board = new Board(match, boardConfiguration, EntityTestFixtures.createPictureSetForBoard(boardConfiguration.getTotalPics()));

        // Then
        assertThat(board.getBoardState().values(), everyItem(hasProperty("guessed", equalTo(false))));
    }
}